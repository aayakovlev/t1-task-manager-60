//package ru.t1.aayakovlev.tm.repository;
//
//import liquibase.Liquibase;
//import liquibase.exception.LiquibaseException;
//import lombok.SneakyThrows;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.*;
//import org.junit.experimental.categories.Category;
//import ru.t1.aayakovlev.tm.MeasureHelper;
//import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
//import ru.t1.aayakovlev.tm.exception.AbstractException;
//import ru.t1.aayakovlev.tm.marker.UnitCategory;
//import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
//import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;
//import ru.t1.aayakovlev.tm.repository.dto.impl.SessionDTORepositoryImpl;
//import ru.t1.aayakovlev.tm.service.ConnectionService;
//import ru.t1.aayakovlev.tm.service.PropertyService;
//import ru.t1.aayakovlev.tm.service.dto.SessionDTOService;
//import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
//import ru.t1.aayakovlev.tm.service.dto.impl.SessionDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
//import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;
//import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;
//
//@Category(UnitCategory.class)
//public final class SessionRepositoryImplTest extends AbstractSchemeTest {
//
//    @NotNull
//    private static PropertyService propertyService;
//
//    @NotNull
//    private static ConnectionService connectionService;
//
//    @NotNull
//    private static EntityManager entityManager;
//
//    @Nullable
//    private static UserDTOService userService;
//
//    @NotNull
//    private static SessionDTORepository repository;
//
//    @NotNull
//    private static SessionDTOService service;
//
//    @BeforeClass
//    public static void initConnectionService() throws LiquibaseException {
//        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
//        liquibase.dropAll();
//        liquibase.update("scheme");
//
//        propertyService = new PropertyServiceImpl();
//        connectionService = new ConnectionServiceImpl(propertyService);
//        entityManager = connectionService.getEntityManager();
//        repository = new SessionDTORepositoryImpl(entityManager);
//        service = new SessionDTOServiceImpl(connectionService);
//        userService = new UserDTOServiceImpl(connectionService, propertyService);
//    }
//
//    @AfterClass
//    public static void destroyConnection() {
//        connectionService.close();
//    }
//
//    @Before
//    public void initData() throws AbstractException {
//        userService.save(COMMON_USER_ONE);
//        userService.save(ADMIN_USER_ONE);
//        service.save(SESSION_USER_ONE);
//        service.save(SESSION_USER_TWO);
//    }
//
//    @After
//    public void after() throws AbstractException {
//        service.clear();
//        userService.clear();
//    }
//
//    @Test
//    public void When_FindByIdExistsSession_Expect_ReturnProject() {
//        @Nullable final SessionDTO project = repository.findById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(SESSION_USER_ONE.getUserId(), project.getUserId());
//    }
//
//    @Test
//    public void When_FindByIdExistsSession_Expect_ReturnNull() {
//        @Nullable final SessionDTO session = repository.findById(USER_ID_NOT_EXISTED, SESSION_ID_NOT_EXISTED);
//        Assert.assertNull(session);
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_SaveNotNullSession_Expect_ReturnProject() {
//        MeasureHelper.measure(entityManager, () -> repository.save(SESSION_ADMIN_ONE));
//        @Nullable final SessionDTO session = repository.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE.getId());
//        Assert.assertNotNull(session);
//        Assert.assertEquals(SESSION_ADMIN_ONE.getUserId(), session.getUserId());
//    }
//
//    @Test
//    public void When_CountCommonUserSessions_Expect_ReturnTwo() {
//        final int count = repository.count(COMMON_USER_ONE.getId());
//        Assert.assertEquals(2, count);
//    }
//
//    @Test
//    public void When_FindAllUserId_Expected_ReturnListSessions() {
//        final List<SessionDTO> sessions = repository.findAll(COMMON_USER_ONE.getId());
//        for (int i = 0; i < sessions.size(); i++) {
//            Assert.assertEquals(USER_SESSION_LIST.get(i).getId(), sessions.get(i).getId());
//        }
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_RemoveExistedSession_Expect_FindNull() {
//        MeasureHelper.measure(entityManager, () -> repository.save(SESSION_ADMIN_THREE));
//        @Nullable final SessionDTO session = repository.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_THREE.getId());
//        Assert.assertNotNull(session);
//
//        MeasureHelper.measure(entityManager, () -> repository.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_THREE.getId()));
//        @Nullable final SessionDTO sessionRemoved = repository.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_THREE.getId());
//        Assert.assertNull(sessionRemoved);
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_RemoveAll_Expect_ZeroCountSessions() {
//        MeasureHelper.measure(entityManager, () -> repository.clear(ADMIN_USER_ONE.getId()));
//        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
//    }
//
//    @Test
//    public void When_RemoveByIdNotExistedSession_Expect_Nothing() {
//        MeasureHelper.measure(entityManager, () -> repository.removeById(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED.getId()));
//    }
//
//}
