package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.enumerated.Role;


public interface UserDTORepository extends BaseDTORepository<UserDTO> {

    @NotNull
    UserDTO create(
            @NotNull final String login,
            @NotNull final String password
    ) throws Exception;

    @NotNull
    UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) throws Exception;

    @NotNull
    UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) throws Exception;

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

    boolean isLoginExists(@NotNull final String login);

    boolean isEmailExists(@NotNull final String email);

}
