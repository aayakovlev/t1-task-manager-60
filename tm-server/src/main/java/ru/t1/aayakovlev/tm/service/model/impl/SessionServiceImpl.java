package ru.t1.aayakovlev.tm.service.model.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;
import ru.t1.aayakovlev.tm.service.model.SessionService;

@Service
public class SessionServiceImpl extends AbstractExtendedService<Session, SessionRepository>
        implements SessionService {

    @Getter
    @NotNull
    @Autowired
    private SessionRepository repository;

}
