package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.User;

public interface UserRepository extends BaseRepository<User> {

    @NotNull
    User create(
            @NotNull final String login,
            @NotNull final String password
    );

    @NotNull
    User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    );

    @NotNull
    User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    );

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    boolean isLoginExists(@NotNull final String login);

    boolean isEmailExists(@NotNull final String email);

}
