package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.model.BaseRepository;
import ru.t1.aayakovlev.tm.service.model.BaseService;

import java.util.*;

@Service
public abstract class AbstractBaseService<E extends AbstractModel, R extends BaseRepository<E>>
        implements BaseService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Nullable
    protected abstract BaseRepository<E> getRepository();

    @NotNull
    @Override
    @Transactional
    public E save(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        return getRepository().save(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<E> add(@Nullable final Collection<E> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        for (@NotNull final E entity : models) {
            @NotNull final E resultEntity = getRepository().save(entity);
            resultEntities.add(resultEntity);
        }
        return resultEntities;
    }

    @Override
    @Transactional
    public void clear() throws AbstractException {
        getRepository().clear();
    }

    @Override
    public int count() throws AbstractException {
        return getRepository().count();

    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        return getRepository().existById(id);
    }

    @NotNull
    @Override
    public List<E> findAll() throws AbstractException {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Comparator<E> comparator) throws AbstractException {
        return getRepository().findAll(comparator);
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable E model = getRepository().findById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    @Transactional
    public void remove(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        removeById(model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        getRepository().removeById(id);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<E> set(@Nullable final Collection<E> models) throws AbstractException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return add(models);
    }

    @NotNull
    @Override
    @Transactional
    public E update(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        return getRepository().update(model);
    }

}
