package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;
import ru.t1.aayakovlev.tm.service.dto.SessionDTOService;

@Service
public class SessionDTOServiceImpl extends AbstractExtendedDTOService<SessionDTO, SessionDTORepository>
        implements SessionDTOService {

    @Getter
    @NotNull
    @Autowired
    private SessionDTORepository repository;

}
