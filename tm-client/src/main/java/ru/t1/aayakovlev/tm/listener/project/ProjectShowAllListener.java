package ru.t1.aayakovlev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.request.ProjectShowAllRequest;
import ru.t1.aayakovlev.tm.dto.response.ProjectShowAllResponse;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class ProjectShowAllListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Show all projects";

    @NotNull
    public static final String NAME = "project-show-all";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectShowAllListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW ALL PROJECTS]");
        System.out.println("Sorts to enter:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final ProjectShowAllRequest request = new ProjectShowAllRequest(getToken());
        request.setSort(sort);

        @Nullable final ProjectShowAllResponse response = projectEndpoint.showAllProjects(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @NotNull final List<ProjectDTO> projects = response.getProjects();
        renderProjects(projects);
    }

}
