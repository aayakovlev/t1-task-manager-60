package ru.t1.aayakovlev.tm.service.impl;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.service.TokenService;

@Getter
@Setter
@Service
public final class TokenServiceImpl implements TokenService {

    @Nullable
    private String token;

}
